from numpy import *
import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt
import sys

def find_best_interval(xs, ys, k):
    assert all(array(xs) == array(sorted(xs))), "xs must be sorted!"

    xs = array(xs)
    ys = array(ys)
    m = len(xs)
    P = [[None for j in range(k+1)] for i in range(m+1)]
    E = zeros((m+1, k+1), dtype=int)

    # Calculate the cumulative sum of ys, to be used later
    cy = concatenate([[0], cumsum(ys)])

    # Initialize boundaries:
    # The error of no intervals, for the first i points
    E[:m+1,0] = cy

    # The minimal error of j intervals on 0 points - always 0. No update needed.

    # Fill middle
    for i in range(1, m+1):
        for j in range(1, k+1):
            # The minimal error of j intervals on the first i points:

            # Exhaust all the options for the last interval. Each interval boundary is marked as either
            # 0 (Before first point), 1 (after first point, before second), ..., m (after last point)
            options = []
            for l in range(0,i+1):
                next_errors = E[l,j-1] + (cy[i]-cy[l]) + concatenate([[0], cumsum((-1)**(ys[arange(l, i)] == 1))])
                min_error = argmin(next_errors)
                options.append((next_errors[min_error], (l, arange(l,i+1)[min_error])))

            E[i,j], P[i][j] = min(options)

    # Extract best interval set and its error count
    best = []
    cur = P[m][k]
    for i in range(k,0,-1):
        best.append(cur)
        cur = P[cur[0]][i-1]
        if cur == None:
            break
    best = sorted(best)
    besterror = E[m,k]

    # Convert interval boundaries to numbers in [0,1]
    exs = concatenate([[0], xs, [1]])
    representatives = (exs[1:]+exs[:-1]) / 2.0
    intervals = [(representatives[l], representatives[u]) for l,u in best]

    return intervals, besterror

def generate_samples(m):
    xs = sorted([random.uniform(0,1) for i in range(m)])
    ys = []
    for x in xs:
        coin = random.uniform(0,1)
        if (x <= 0.25 or (x >= 0.5 and x <= 0.75)):
            ys += [(coin <= 0.8)]
        else:
            ys += [(coin <= 0.1)]

    return xs, ys

def is_included(x, interval):
    return x >= interval[0] and x <= interval[1]

def calculate_holdout_test_error(xs, ys, intervals):
    err = 0.0
    for i in range(len(xs)):
        in_interval = False
        for interval in intervals:
            if(is_included(xs[i], interval)):
                in_interval = True
                break
        if(ys[i] != in_interval):
            err += 1
    return err

def calculate_true_error(intervals):
    err = 0.0
    intervals = [0.0] + sorted([interval[i] for interval in intervals for i in range(2)]) + [1.0]
    i = 0

    # Case 1:   0.0 <= intervals[i] < 0.25
    while(intervals[i] < 0.25):
        X = intervals[i+1]
        lastX = intervals[i]
        i += 1
        P = 0.8 if i%2 else 0.2
        err += P * (min(X, 0.25) - lastX)

    P = 0.1 if i%2 else 0.9
    err += P * (min(X, 0.5) - 0.25)

    # Case 2:   0.25 <= intervals[i] < 0.5
    while(intervals[i] < 0.5):
        X = intervals[i+1]
        lastX = intervals[i]
        i += 1
        P = 0.1 if i%2 else 0.9
        err += P * (min(X, 0.5) - lastX)

    P = 0.8 if i%2 else 0.2
    err += P * (min(X, 0.75) - 0.5)

    # Case 3:   0.5 <= intervals[i] < 0.75
    while(intervals[i] < 0.75):
        X = intervals[i+1]
        lastX = intervals[i]
        i += 1
        P = 0.8 if i%2 else 0.2
        err += P * (min(X, 0.75) - lastX)

    P = 0.1 if i%2 else 0.9
    err += P * (min(X, 1) - 0.75)

    # Case 4:   0.75 <= intervals[i] < 1.0
    while(intervals[i] < 1):
        X = intervals[i+1]
        lastX = intervals[i]
        i += 1
        P = 0.1 if i%2 else 0.9
        err += P * (min(X, 1) - lastX)

    return err

def Qa():
    # Parameters
    m = 100
    k = 2

    # Generating random sample
    xs, ys = generate_samples(m)

    fig = plt.figure()
    plt.plot(xs, ys, "o")
    plt.plot([0.25, 0.25], [-0.1, 1.1], "--")
    plt.plot([0.5, 0.5], [-0.1, 1.1], "--")
    plt.plot([0.75, 0.75], [-0.1, 1.1], "--")
    plt.ylim([-0.1, 1.1])

    intervals, error = find_best_interval(xs,ys,k)
    print("Empirical error: {0}".format(error))

    for i in intervals:
        plt.plot([i[0], i[1]], [0.5, 0.5])

    # plt.show()
    fig.savefig("Qa.png")
    plt.close()

def Qc():
    T = 100
    k = 2
    Ms = [(5*i + 10) for i in range(19)]
    true_errors = []
    empirical_errors = []
    for m in Ms:
        t_e = 0.0
        e_e = 0.0
        for i in range(T):
            xs, ys = generate_samples(m)
            intervals, curr_ee = find_best_interval(xs, ys, k)
            e_e += curr_ee
            t_e += calculate_true_error(intervals)
        true_errors += [t_e / T]
        empirical_errors += [e_e / T / m]
    fig = plt.figure()
    plt.plot(Ms, empirical_errors, label='Empirical error')
    plt.plot(Ms, true_errors, label='True error')
    plt.grid()
    plt.legend()
    # plt.show()
    fig.savefig("Qc.png")
    plt.close()

def Qd():
    m = 50
    Ks = [i+1 for i in range(20)]
    xs, ys = generate_samples(m)
    true_errors = []
    empirical_errors = []
    for k in Ks:
        intervals, curr_ee = find_best_interval(xs, ys, k)
        true_errors += [calculate_true_error(intervals)]
        empirical_errors += [curr_ee / m]
    fig = plt.figure()
    plt.plot(Ks, empirical_errors, label='Empirical error')
    plt.plot(Ks, true_errors, label='True error')
    plt.grid()
    plt.legend()
    # plt.show()
    fig.savefig("Qd.png")
    plt.close()

    return xs, ys

def Qe():
    m = 50
    train_Xs, train_Ys = generate_samples(m)
    test_Xs, test_Ys = Qd()
    Ks = [i+1 for i in range(20)]
    true_errors = []
    for k in Ks:
        intervals, curr_err = find_best_interval(train_Xs, train_Ys, k)
        err = calculate_holdout_test_error(test_Xs, test_Ys, intervals)
        true_errors += [err / m]
    fig = plt.figure()
    plt.plot(Ks, true_errors, label='True error')
    plt.grid()
    plt.legend()
    # plt.show()
    fig.savefig("Qe.png")
    plt.close()

def Qe_():
    Qd()
    Qe()

# MAIN #
subsections = {"-a" : Qa,
               "-c" : Qc,
               "-d" : Qd,
               "-e" : Qe_}

subsections[sys.argv[1]]()
