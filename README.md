NAME            I.D.        E-MAIL                      USERNAME
----            ----        ------                      --------
Assaf Avital    204097588   assafavital9@gmail.com      assafavital
Yahav Avigal    200921740   yahavigal@gmail.com         yahavavigal

Plots path: /   (relative to code path)
Plots are saved as 'Q<subsection>.png'

COMMAND-LINE ARGUMENTS:
    "-a"    :   Execute subsection A
    "-c"    :   Execute subsection C
    "-d"    :   Execute subsection D
    "-e"    :   Execute subsection E

Running subsection A for example:   "python ML2.py -a"

Code path:  /specific/a/home/cc/students/cs/assafavital/ml2